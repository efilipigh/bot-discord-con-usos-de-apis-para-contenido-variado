import discord
import datetime
import requests
import re
import json
import random
from discord import embeds, colour
from discord.ext import commands
from urllib import parse, request
from url_apis import *
from libgen_api import LibgenSearch

bot_token = "aca hay que poner la api del bot que provee discord a cada desarrollador"

bot = commands.Bot(command_prefix='!', description="Esto es un bot.")

#descargar libros de LibGen
def nom_lib(n1):
    nom_lib = LibgenSearch()
    res = nom_lib.search_title(n1)

    res_json_data = json.dumps(res)
    res_item_dict = json.loads(res_json_data)

    ayuda = "Podes descargar tu libro de la siguiente direccion: "
    #print(ayuda)
    fuentes = []
    for i in range(1, len(res_item_dict)):
        for z in range(1, 6):
            if res[i][f'Mirror_{z}'] != None:
                fuente = res[i][f'Mirror_{z}']
                fuentes.append(fuente)
                str_final = str(ayuda+random.choice(fuentes))
                msj_error = "No hubo resultados"
                try:
                    return str_final
                except Exception:
                    return msj_error
                except IndexError:
                    return msj_error


def nom_autor_lib_rnd(n1):
    autor = LibgenSearch()
    rez = autor.search_author(n1)

    rez_json_data = json.dumps(rez)
    rez_item_dict = json.loads(rez_json_data)

    ayuda = "Podes descargar uno de sus libros de la siguiente direccion: "
    fuentez = []
    for i in range(len(rez_item_dict)):
        for z in range(1, 5):
            fuente_ = rez[i][f'Mirror_{z}']
            fuentez.append(fuente_)
            str_final_2 = str(ayuda+random.choice(fuentez))
            msj_error = "No hubo resultados"
            try:
                return str_final_2
            except Exception:
                return msj_error
            except IndexError:
                return msj_error
    #return ayuda, random.choice(fuentez)

def chuck_norris_(orig):
    r = requests.get(chuck_norris_api).json()
    chuck_norris_r_2 = r['value']
    return chuck_norris_r_2


def chuck_norris_():
    r = requests.get(chuck_norris_api).json()
    chuck_norris_r_2 = r['value']
    return chuck_norris_r_2


def no_existe_img():
    r = requests.get(persona_no_existe_api).json()
    fake_perso = r['image_url']
    return fake_perso


def perritos_rand():
    r = requests.get(perro_api).json()
    rope = r['message']
    return rope


def humor_negro_():
    """r = requests.get(humor_negro).json()
    humor_negro_r = r['meme']
    msj_error = "No hubo resultados"
    error_server = "El servidor de los memes esta caido..."
    try:
        return humor_negro_r
    except Exception:
        return msj_error
    except IndexError:
        return msj_error
    except discord.errors.HTTPException:
        return error_server
    except discord.HTTPException:
        return error_server
    finally:"""
    error_server = "El servidor de los memes esta caido..."
    return error_server
    #return humor_negro_r


def foto_random_grande():
    r = requests.get(api_rand).json()
    foto_random_ = r['results'][0]['picture']['large']
    return foto_random_


def memes_1():
    r = requests.get(memes_1_api).json()
    meme_1_url = r[0]['url']
    return meme_1_url


def memes_2():
    r = requests.get(memes_2_api).json()
    meme_2_url = r['url']
    return meme_2_url


def memes_3():
    r = requests.get(memes_3_api).json()
    meme_3_r = r['meme']
    return meme_3_r


def ejecutar_basicos():
    r = requests.get(api_rand).json()
    datos_basico = []  # donde vamos a guarda en diccionario
    # genero, nombre, apellido y fecha de nacimiento y ubicacion
    basico = {
        'genero': r['results'][0]['gender'],
        'titulo': r['results'][0]['name']['title'],
        'nombre': r['results'][0]['name']['first'],
        'apellido': r['results'][0]['name']['last'],
        'fecha_nac': r['results'][0]['dob']['date'],
        'edad': r['results'][0]['dob']['age'],
        'calle': r['results'][0]['location']['street']['name'],
        'numero': r['results'][0]['location']['street']['number'],
        'ciudad': r['results'][0]['location']['city'],
        'estado': r['results'][0]['location']['state'],
        'pais': r['results'][0]['location']['country'],
        'codigo_postal': r['results'][0]['location']['postcode'],
        'nacionalidad': r['results'][0]['nat'],
    }
    # aca los pasamos a un diccionario
    datos_basico.append(basico)
    return datos_basico


def ejecutar_medios():
    r = requests.get(api_rand).json()
    datos_medios = []
    # mas especificos, email, usuarios y password
    medios = {
        'telefono': r['results'][0]['phone'],
        'celular': r['results'][0]['cell'],
        'foto_grande': r['results'][0]['picture']['large'],
        'foto_mediana': r['results'][0]['picture']['medium'],
        'foto_chica': r['results'][0]['picture']['thumbnail'],
    }
    # aca los pasamos a un diccionario
    datos_medios.append(medios)
    return datos_medios

# api de temperatura


def temperatura_consulta(ciudad):
    #api temperatura
    url_temp = f'http://api.openweathermap.org/data/2.5/weather?q={ ciudad }&units=metric&appid=c13484871c411be98bd62cdb562ede42'
    r = requests.get(url_temp).json()
    ciudad_ = ciudad
    termica = r['main']['feels_like']
    temp_actual = r['main']['temp']
    temperatura_min = r['main']['temp_min']
    temperatura_max = r['main']['temp_max']
    presion_atmos = r['main']['pressure']
    descripcion = r['weather'][0]['description']
    icono = r['weather'][0]['icon']

    r_completo = f'En la ciudad de {ciudad_} hace:  {temp_actual} grados, la termica actual es de:{ termica }°, la temperatura minima para hoy es de: {temperatura_min} \
        °, la temperatura maxima para hoy es de:{temperatura_max}°, la presion atmosferica es de:{presion_atmos } hPa'

    r_completo_str = str(r_completo)

    return r_completo_str


def icono_temp(ciudad):
    #api temperatura
    url_temp = f'http://api.openweathermap.org/data/2.5/weather?q={ ciudad }&units=metric&appid=c13484871c411be98bd62cdb562ede42'
    r = requests.get(url_temp).json()
    icono = r['weather'][0]['icon']
    print(icono)
    return icono


    

@bot.command()
async def buscar_libro_autor(ctx, libro):
    embed = discord.Embed(colour=discord.Colour.red())
    libro_autor = nom_autor_lib_rnd(libro)
    #url_icono = icono_temp(ciudad)
    embed.add_field(name="Resultado", value=libro_autor)
    # embed.set_image(url=url_icono)
    await ctx.send(embed=embed)

@bot.command()
async def buscar_libro_nom(ctx, libro):
    embed = discord.Embed(colour=discord.Colour.red())
    libro_nom = nom_lib(libro)
    #url_icono = icono_temp(ciudad)
    embed.add_field(name="Resultado", value=libro_nom)
    # embed.set_image(url=url_icono)
    await ctx.send(embed=embed)


# comando temperatura


@bot.command()
async def temperatura(ctx, ciudad):
    embed = discord.Embed(colour=discord.Colour.red())
    temp_res = temperatura_consulta(ciudad)
    url_icono = icono_temp(ciudad)
    embed.add_field(name="Resultado", value=temp_res)
    # embed.set_image(url=url_icono)
    await ctx.send(embed=embed)


# comando troll
@bot.command()
async def hacerte_admin(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    embed2 = discord.Embed(colour=discord.Colour.red())
    #url_troll = "https://thumbs.gfycat.com/ImpracticalTheseDingo-size_restricted.gif"
    #url_troll2 = "https://j.gifs.com/v1wz7E.gif"
    #embed.add_field(name="Admin", value="Hacerte admin del server")
    embed.set_image(url=url_troll)
    embed2.set_image(url=url_troll2)
    await ctx.send(embed=embed)
    await ctx.send(embed=embed2)

# chuck norris


@bot.command()
async def chuck_norris(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    chuck_norris_frase = chuck_norris_()
    embed.add_field(name="Frases celebres de Chuck Norris",
                    value=chuck_norris_frase)
    # await ctx.send(chuck_norris_())
    await ctx.send(embed=embed)

# perritos random


@bot.command()
async def perritos(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    perritos_url = perritos_rand()
    embed.set_image(url=perritos_url)
    await ctx.send(embed=embed)

# personas inexistentes


@bot.command()
async def no_existe(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    no_existe_img_url = no_existe_img()
    embed.set_image(url=no_existe_img_url)
    await ctx.send(embed=embed)

# comandos de los memes


@bot.command()
async def meme_1(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    meme_1_url = memes_1()
    embed.set_image(url=meme_1_url)
    await ctx.send(embed=embed)


@bot.command()
async def meme_2(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    meme_2_url = memes_2()
    embed.set_image(url=meme_2_url)
    await ctx.send(embed=embed)


@bot.command()
async def meme_3(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    meme_3_url = memes_3()
    embed.set_image(url=meme_3_url)
    await ctx.send(embed=embed)


@bot.command()
async def dank_meme(ctx):
    embed = discord.Embed(colour=discord.Colour.red())
    dank_meme_url = dank_meme()
    embed.set_image(url=dank_meme_url)
    await ctx.send(embed=embed)
# fin comandos memes


@ bot.command()
async def ayuda(ctx):
    embed = discord.Embed(
        title="Estos son mis comandos de ayuda", colour=discord.Colour.red())
    embed.add_field(name="!ayuda", value="Devuelve este menu")
    embed.add_field(
        name="!basico", value="Devuelve un diccionario con datos de personas inexistentes")
    embed.add_field(
        name="!medio", value="Devuelve datos complementarios a !basicos")
    embed.add_field(name="!sum num1 num2",
                    value="Devuelve la suma de dos numeros")
    embed.add_field(name="!buscar_libro_nom 'Nombre del Libro'",
                    value="Devuelve una URL para bajar ese libro")
    embed.add_field(name="!buscar_libro_autor 'Nombre del autor'",
                    value="Devuelve una URL para bajar ese libro")
    embed.add_field(
        name="!info", value="Devuelve informacion sobre el servidor")
    embed.add_field(name="!meme_1", value="Devuelve memes de la india")
    embed.add_field(name="!meme_2", value="Devuelve memes de un origen")
    embed.add_field(name="!meme_3", value="Devuelve memes de otro origen")
    embed.add_field(name="!dank_meme", value="Devuelve dank memes")
    embed.add_field(name="!foto_random",
                    value="Devuelve fotos aleatorias de personas no reales")
    embed.add_field(name="!no_existe",
                    value="Devuelve fotos aleatorias de personas que no existen")
    embed.add_field(name="!perritos",
                    value="Devuelve fotos aleatorias de perritos!")
    embed.add_field(name="!temperatura ciudad",
                    value="Devuelve datos sobre la temperatura de ese lugar.")
    embed.add_field(name="!chuck_norris",
                    value="Devuelve frases de Chuck Norris")
    embed.add_field(name="!hacerte_admin", value="Te hace admin del server")
    embed.set_thumbnail(url=f"{ctx.guild.icon}")
    embed.set_thumbnail(
        url="https://pluralsight.imgix.net/paths/python-7be70baaac.png")
    await ctx.send(embed=embed)


@ bot.command()
async def foto_random(ctx):
    await ctx.send(foto_random_grande())


@ bot.command()
async def basico(ctx):
    await ctx.send(ejecutar_basicos())


@ bot.command()
async def medio(ctx):
    await ctx.send(ejecutar_medios())


@ bot.command()
async def sum(ctx, numOne: int, numTwo: int):
    await ctx.send(numOne + numTwo)


@ bot.command()
async def info(ctx):
    embed = discord.Embed(title=f"{ctx.guild.name}", description="Lorem Ipsum asdasd",
                          timestamp=datetime.datetime.utcnow(), color=discord.Color.blue())
    embed.add_field(name="Server created at", value=f"{ctx.guild.created_at}")
    embed.add_field(name="Server Owner", value=f"{ctx.guild.owner}")
    embed.add_field(name="Server Region", value=f"{ctx.guild.region}")
    embed.add_field(name="Server ID", value=f"{ctx.guild.id}")
    # embed.set_thumbnail(url=f"{ctx.guild.icon}")
    embed.set_thumbnail(
        url="https://pluralsight.imgix.net/paths/python-7be70baaac.png")

    await ctx.send(embed=embed)

# Events


@ bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Streaming(name="No molestar wachin", url="http://www.twitch.tv/a_tu_casa_perro"))
    print('Running')

bot.run(bot_token)
