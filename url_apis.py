# api valores de exchange
api_exchange = "https://currency-exchange.p.rapidapi.com/exchange"
# api chuck norris
chuck_norris_api = "https://api.chucknorris.io/jokes/random"
# api persona inexistente
persona_no_existe_api = "https://fakeface.rest/face/json"
# api de memes humor negro
humor_negro = "https://meme-api.glitch.me/dank"
# api de datos random
api_rand = "https://randomuser.me/api/"
# api de perritos random
perro_api = "https://dog.ceo/api/breeds/image/random"
# aca vamos a definir la api de los memes wachin, el numero 1 al final hace referencia a la cantidad de memes aleatorios que se generaran
memes_1_api = "https://namo-memes.herokuapp.com/memes/1"
# aca vamos a definir la api de los memes wachin, el numero 1 al final hace referencia a la cantidad de memes aleatorios que se generaran
memes_2_api = "https://meme-api.herokuapp.com/gimme"
# api memes 3
memes_3_api = "https://meme-api.glitch.me/sbubby"
#troll urls
url_troll = "https://thumbs.gfycat.com/ImpracticalTheseDingo-size_restricted.gif"
url_troll2 = "https://j.gifs.com/v1wz7E.gif"